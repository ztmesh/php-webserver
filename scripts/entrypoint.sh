# Start supervisord and services
if [[ -f "/beforestart.sh" ]]; then
	sh /beforestart.sh
fi
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
